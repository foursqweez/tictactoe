package com.example.tic_tac_toe

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("UNREACHABLE_CODE")
class MainActivity : AppCompatActivity() {

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt3.setOnClickListener {
            val intent = Intent(this, BoardActivity::class.java)
            intent.putExtra("dimension", "3")
            startActivity(intent)
        }

        bt5.setOnClickListener {
            val intent = Intent(this, BoardActivity::class.java)
            intent.putExtra("dimension", "5")
            startActivity(intent)
        }

        bt7.setOnClickListener {
            val intent = Intent(this, BoardActivity::class.java)
            intent.putExtra("dimension", "7")
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}


