package com.example.tic_tac_toe

import android.util.Log

@Suppress("UNREACHABLE_CODE")
class Board(toInt: Int) {

    companion object {
        const val PLAYER_1 = "O"
        const val PLAYER_2 = "X"
        const val TAG = "board"
    }

    val board = Array(toInt) { arrayOfNulls<String>(toInt) }

    //a list of all the empty cells
    private val availableCells: List<Cell>
        get() {
            val cells = mutableListOf<Cell>()
            for (i in board.indices) {
                for (j in board.indices) {
                    if (board[i][j].isNullOrEmpty()) {
                        cells.add(Cell(i, j))
                    }
                }
            }
            return cells
        }

    //this property will tell
    //if the game is over or not
    val isGameOver: Boolean
        get() {
            return hasPlayer2Won() || hasPlayer1Won() || availableCells.isEmpty()
        }


    fun hasPlayer1Won(): Boolean {
        when (board.size) {
            3 -> {

                Log.i(TAG, "" + board.size)
                if (board[0][0] == board[1][1] &&
                    board[0][0] == board[2][2] &&
                    board[0][0] == PLAYER_1 ||
                    board[0][2] == board[1][1] &&
                    board[0][2] == board[2][0] &&
                    board[0][2] == PLAYER_1
                ) {
                    return true
                }

                for (i in board.indices) {
                    if (
                        board[i][0] == board[i][1] &&
                        board[i][0] == board[i][2] &&
                        board[i][0] == PLAYER_1 ||
                        board[0][i] == board[1][i] &&
                        board[0][i] == board[2][i] &&
                        board[0][i] == PLAYER_1
                    ) {
                        return true
                    }
                }
            }
            5 -> {
                Log.i(TAG, "" + board.size)
                if (board[0][0] == board[1][1] &&
                    board[0][0] == board[2][2] &&
                    board[0][0] == board[3][3] &&
                    board[0][0] == PLAYER_1 ||

                    board[1][1] == board[2][2] &&
                    board[1][1] == board[3][3] &&
                    board[1][1] == board[4][4] &&
                    board[1][1] == PLAYER_1 ||

                    board[0][4] == board[1][3] &&
                    board[0][4] == board[2][2] &&
                    board[0][4] == board[3][1] &&
                    board[0][4] == PLAYER_1 ||

                    board[1][3] == board[4][0] &&
                    board[1][3] == board[2][2] &&
                    board[1][3] == board[3][1] &&
                    board[1][3] == PLAYER_1 ||

                    board[0][1] == board[1][2] &&
                    board[0][1] == board[2][3] &&
                    board[0][1] == board[3][4] &&
                    board[0][1] == PLAYER_1 ||

                    board[1][0] == board[2][1] &&
                    board[1][0] == board[3][2] &&
                    board[1][0] == board[4][3] &&
                    board[1][0] == PLAYER_1 ||

                    board[0][3] == board[1][2] &&
                    board[0][3] == board[2][1] &&
                    board[0][3] == board[3][0] &&
                    board[0][3] == PLAYER_1 ||

                    board[1][4] == board[2][3] &&
                    board[1][4] == board[3][2] &&
                    board[1][4] == board[4][1] &&
                    board[1][4] == PLAYER_1

                ) {
                    return true
                }
                for (i in board.indices) {
                    if (
                        board[i][0] == board[i][1] &&
                        board[i][0] == board[i][2] &&
                        board[i][0] == board[i][3] &&
                        board[i][0] == PLAYER_1 ||

                        board[i][1] == board[i][2] &&
                        board[i][1] == board[i][3] &&
                        board[i][1] == board[i][4] &&
                        board[i][1] == PLAYER_1 ||


                        board[0][i] == board[1][i] &&
                        board[0][i] == board[2][i] &&
                        board[0][i] == board[3][i] &&
                        board[0][i] == PLAYER_1 ||

                        board[1][i] == board[2][i] &&
                        board[1][i] == board[3][i] &&
                        board[1][i] == board[4][i] &&
                        board[1][i] == PLAYER_1

                    ) {
                        return true
                    }
                }
            }
            7 -> {
                Log.i(TAG, "" + board.size)
                if (board[0][0] == board[1][1] &&
                    board[0][0] == board[2][2] &&
                    board[0][0] == board[3][3] &&
                    board[0][0] == PLAYER_1 ||

                    board[1][1] == board[2][2] &&
                    board[1][1] == board[3][3] &&
                    board[1][1] == board[4][4] &&
                    board[1][1] == PLAYER_1 ||

                    board[2][2] == board[3][3] &&
                    board[2][2] == board[4][4] &&
                    board[2][2] == board[5][5] &&
                    board[2][2] == PLAYER_1 ||

                    board[3][3] == board[4][4] &&
                    board[3][3] == board[5][5] &&
                    board[3][3] == board[6][6] &&
                    board[3][3] == PLAYER_1 ||

                    board[0][6] == board[1][5] &&
                    board[0][6] == board[2][4] &&
                    board[0][6] == board[3][3] &&
                    board[0][6] == PLAYER_1 ||

                    board[1][5] == board[2][4] &&
                    board[1][5] == board[3][3] &&
                    board[1][5] == board[4][2] &&
                    board[1][5] == PLAYER_1 ||

                    board[2][4] == board[3][3] &&
                    board[2][4] == board[4][2] &&
                    board[2][4] == board[5][1] &&
                    board[2][4] == PLAYER_1 ||

                    board[3][3] == board[4][2] &&
                    board[3][3] == board[5][1] &&
                    board[3][3] == board[6][0] &&
                    board[3][3] == PLAYER_1 ||

                    board[0][1] == board[1][2] &&
                    board[0][1] == board[2][3] &&
                    board[0][1] == board[3][4] &&
                    board[0][1] == PLAYER_1 ||

                    board[1][2] == board[2][3] &&
                    board[1][2] == board[3][4] &&
                    board[1][2] == board[4][5] &&
                    board[1][2] == PLAYER_1 ||

                    board[2][3] == board[3][4] &&
                    board[2][3] == board[4][5] &&
                    board[2][3] == board[5][6] &&
                    board[2][3] == PLAYER_1 ||

                    board[0][2] == board[1][3] &&
                    board[0][2] == board[2][4] &&
                    board[0][2] == board[3][5] &&
                    board[0][2] == PLAYER_1 ||

                    board[1][3] == board[2][4] &&
                    board[1][3] == board[3][5] &&
                    board[1][3] == board[4][6] &&
                    board[1][3] == PLAYER_1 ||

                    board[0][3] == board[1][4] &&
                    board[0][3] == board[2][5] &&
                    board[0][3] == board[3][6] &&
                    board[0][3] == PLAYER_1 ||

                    board[1][0] == board[2][1] &&
                    board[1][0] == board[3][2] &&
                    board[1][0] == board[4][3] &&
                    board[1][0] == PLAYER_1 ||

                    board[2][1] == board[3][2] &&
                    board[2][1] == board[4][3] &&
                    board[2][1] == board[5][4] &&
                    board[2][1] == PLAYER_1 ||

                    board[3][2] == board[4][3] &&
                    board[3][2] == board[5][4] &&
                    board[3][2] == board[6][5] &&
                    board[3][2] == PLAYER_1 ||

                    board[2][0] == board[3][1] &&
                    board[2][0] == board[4][2] &&
                    board[2][0] == board[5][3] &&
                    board[2][0] == PLAYER_1 ||

                    board[3][1] == board[4][2] &&
                    board[3][1] == board[5][3] &&
                    board[3][1] == board[6][4] &&
                    board[3][1] == PLAYER_1 ||

                    board[3][0] == board[4][1] &&
                    board[3][0] == board[5][2] &&
                    board[3][0] == board[6][3] &&
                    board[3][0] == PLAYER_1 ||

                    board[0][5] == board[1][4] &&
                    board[0][5] == board[2][3] &&
                    board[0][5] == board[3][2] &&
                    board[0][5] == PLAYER_1 ||

                    board[1][4] == board[2][3] &&
                    board[1][4] == board[3][2] &&
                    board[1][4] == board[4][1] &&
                    board[1][4] == PLAYER_1 ||

                    board[2][3] == board[3][2] &&
                    board[2][3] == board[4][1] &&
                    board[2][3] == board[5][0] &&
                    board[2][3] == PLAYER_1 ||

                    board[0][4] == board[1][3] &&
                    board[0][4] == board[2][2] &&
                    board[0][4] == board[3][1] &&
                    board[0][4] == PLAYER_1 ||

                    board[1][3] == board[2][2] &&
                    board[1][3] == board[3][1] &&
                    board[1][3] == board[4][0] &&
                    board[1][3] == PLAYER_1 ||

                    board[0][3] == board[1][2] &&
                    board[0][3] == board[2][1] &&
                    board[0][3] == board[3][0] &&
                    board[0][3] == PLAYER_1 ||

                    board[1][6] == board[2][5] &&
                    board[1][6] == board[3][4] &&
                    board[1][6] == board[4][3] &&
                    board[1][6] == PLAYER_1 ||

                    board[2][5] == board[3][4] &&
                    board[2][5] == board[4][3] &&
                    board[2][5] == board[5][2] &&
                    board[2][5] == PLAYER_1 ||

                    board[3][4] == board[4][3] &&
                    board[3][4] == board[5][2] &&
                    board[3][4] == board[6][1] &&
                    board[3][4] == PLAYER_1 ||

                    board[2][6] == board[3][5] &&
                    board[2][6] == board[4][4] &&
                    board[2][6] == board[5][3] &&
                    board[2][6] == PLAYER_1 ||

                    board[3][5] == board[4][4] &&
                    board[3][5] == board[5][3] &&
                    board[3][5] == board[6][2] &&
                    board[3][5] == PLAYER_1 ||

                    board[3][6] == board[4][5] &&
                    board[3][6] == board[5][4] &&
                    board[3][6] == board[6][3] &&
                    board[3][6] == PLAYER_1
                ) {
                    return true
                }
                for (i in board.indices) {
                    if (
                        board[i][0] == board[i][1] &&
                        board[i][0] == board[i][2] &&
                        board[i][0] == board[i][3] &&
                        board[i][0] == PLAYER_1 ||

                        board[i][1] == board[i][2] &&
                        board[i][1] == board[i][3] &&
                        board[i][1] == board[i][4] &&
                        board[i][1] == PLAYER_1 ||

                        board[i][2] == board[i][3] &&
                        board[i][2] == board[i][4] &&
                        board[i][2] == board[i][5] &&
                        board[i][2] == PLAYER_1 ||

                        board[i][3] == board[i][4] &&
                        board[i][3] == board[i][5] &&
                        board[i][3] == board[i][6] &&
                        board[i][3] == PLAYER_1 ||


                        board[0][i] == board[1][i] &&
                        board[0][i] == board[2][i] &&
                        board[0][i] == board[3][i] &&
                        board[0][i] == PLAYER_1 ||

                        board[1][i] == board[2][i] &&
                        board[1][i] == board[3][i] &&
                        board[1][i] == board[4][i] &&
                        board[1][i] == PLAYER_1 ||

                        board[2][i] == board[3][i] &&
                        board[2][i] == board[4][i] &&
                        board[2][i] == board[5][i] &&
                        board[2][i] == PLAYER_1 ||

                        board[3][i] == board[4][i] &&
                        board[3][i] == board[5][i] &&
                        board[3][i] == board[6][i] &&
                        board[3][i] == PLAYER_1

                    ) {
                        return true
                    }
                }
            }
        }
        return false
    }

    fun hasPlayer2Won(): Boolean {
        when (board.size) {
            3 -> {
                if (board[0][0] == board[1][1] &&
                    board[0][0] == board[2][2] &&
                    board[0][0] == PLAYER_2 ||
                    board[0][2] == board[1][1] &&
                    board[0][2] == board[2][0] &&
                    board[0][2] == PLAYER_2
                ) {
                    return true
                }

                for (i in board.indices) {
                    if (
                        board[i][0] == board[i][1] &&
                        board[i][0] == board[i][2] &&
                        board[i][0] == PLAYER_2 ||
                        board[0][i] == board[1][i] &&
                        board[0][i] == board[2][i] &&
                        board[0][i] == PLAYER_2
                    ) {
                        return true
                    }
                }
            }
            5 -> {
                if (board[0][0] == board[1][1] &&
                    board[0][0] == board[2][2] &&
                    board[0][0] == board[3][3] &&
                    board[0][0] == PLAYER_2 ||

                    board[1][1] == board[2][2] &&
                    board[1][1] == board[3][3] &&
                    board[1][1] == board[4][4] &&
                    board[1][1] == PLAYER_2 ||

                    board[0][4] == board[1][3] &&
                    board[0][4] == board[2][2] &&
                    board[0][4] == board[3][1] &&
                    board[0][4] == PLAYER_2 ||

                    board[1][3] == board[4][0] &&
                    board[1][3] == board[2][2] &&
                    board[1][3] == board[3][1] &&
                    board[1][3] == PLAYER_2 ||

                    board[0][1] == board[1][2] &&
                    board[0][1] == board[2][3] &&
                    board[0][1] == board[3][4] &&
                    board[0][1] == PLAYER_2 ||

                    board[1][0] == board[2][1] &&
                    board[1][0] == board[3][2] &&
                    board[1][0] == board[4][3] &&
                    board[1][0] == PLAYER_2 ||

                    board[0][3] == board[1][2] &&
                    board[0][3] == board[2][1] &&
                    board[0][3] == board[3][0] &&
                    board[0][3] == PLAYER_2 ||

                    board[1][4] == board[2][3] &&
                    board[1][4] == board[3][2] &&
                    board[1][4] == board[4][1] &&
                    board[1][4] == PLAYER_2
                ) {
                    return true
                }

                for (i in board.indices) {
                    if (
                        board[i][0] == board[i][1] &&
                        board[i][0] == board[i][2] &&
                        board[i][0] == board[i][3] &&
                        board[i][0] == PLAYER_2 ||

                        board[i][1] == board[i][2] &&
                        board[i][1] == board[i][3] &&
                        board[i][1] == board[i][4] &&
                        board[i][1] == PLAYER_2 ||

                        board[0][i] == board[1][i] &&
                        board[0][i] == board[2][i] &&
                        board[0][i] == board[3][i] &&
                        board[0][i] == PLAYER_2 ||

                        board[1][i] == board[2][i] &&
                        board[1][i] == board[3][i] &&
                        board[1][i] == board[4][i] &&
                        board[1][i] == PLAYER_2
                    ) {
                        return true
                    }
                }
            }
            7 -> {
                Log.i(TAG, "" + board.size)
                if (board[0][0] == board[1][1] &&
                    board[0][0] == board[2][2] &&
                    board[0][0] == board[3][3] &&
                    board[0][0] == PLAYER_2 ||

                    board[1][1] == board[2][2] &&
                    board[1][1] == board[3][3] &&
                    board[1][1] == board[4][4] &&
                    board[1][1] == PLAYER_2 ||

                    board[2][2] == board[3][3] &&
                    board[2][2] == board[4][4] &&
                    board[2][2] == board[5][5] &&
                    board[2][2] == PLAYER_2 ||

                    board[3][3] == board[4][4] &&
                    board[3][3] == board[5][5] &&
                    board[3][3] == board[6][6] &&
                    board[3][3] == PLAYER_2 ||

                    board[0][6] == board[1][5] &&
                    board[0][6] == board[2][4] &&
                    board[0][6] == board[3][3] &&
                    board[0][6] == PLAYER_2 ||

                    board[1][5] == board[2][4] &&
                    board[1][5] == board[3][3] &&
                    board[1][5] == board[4][2] &&
                    board[1][5] == PLAYER_2 ||

                    board[2][4] == board[3][3] &&
                    board[2][4] == board[4][2] &&
                    board[2][4] == board[5][1] &&
                    board[2][4] == PLAYER_2 ||

                    board[3][3] == board[4][2] &&
                    board[3][3] == board[5][1] &&
                    board[3][3] == board[6][0] &&
                    board[3][3] == PLAYER_2 ||

                    board[0][1] == board[1][2] &&
                    board[0][1] == board[2][3] &&
                    board[0][1] == board[3][4] &&
                    board[0][1] == PLAYER_2 ||

                    board[1][2] == board[2][3] &&
                    board[1][2] == board[3][4] &&
                    board[1][2] == board[4][5] &&
                    board[1][2] == PLAYER_2 ||

                    board[2][3] == board[3][4] &&
                    board[2][3] == board[4][5] &&
                    board[2][3] == board[5][6] &&
                    board[2][3] == PLAYER_2 ||

                    board[0][2] == board[1][3] &&
                    board[0][2] == board[2][4] &&
                    board[0][2] == board[3][5] &&
                    board[0][2] == PLAYER_2 ||

                    board[1][3] == board[2][4] &&
                    board[1][3] == board[3][5] &&
                    board[1][3] == board[4][6] &&
                    board[1][3] == PLAYER_2 ||

                    board[0][3] == board[1][4] &&
                    board[0][3] == board[2][5] &&
                    board[0][3] == board[3][6] &&
                    board[0][3] == PLAYER_2 ||

                    board[1][0] == board[2][1] &&
                    board[1][0] == board[3][2] &&
                    board[1][0] == board[4][3] &&
                    board[1][0] == PLAYER_2 ||

                    board[2][1] == board[3][2] &&
                    board[2][1] == board[4][3] &&
                    board[2][1] == board[5][4] &&
                    board[2][1] == PLAYER_2 ||

                    board[3][2] == board[4][3] &&
                    board[3][2] == board[5][4] &&
                    board[3][2] == board[6][5] &&
                    board[3][2] == PLAYER_2 ||

                    board[2][0] == board[3][1] &&
                    board[2][0] == board[4][2] &&
                    board[2][0] == board[5][3] &&
                    board[2][0] == PLAYER_2 ||

                    board[3][1] == board[4][2] &&
                    board[3][1] == board[5][3] &&
                    board[3][1] == board[6][4] &&
                    board[3][1] == PLAYER_2 ||

                    board[3][0] == board[4][1] &&
                    board[3][0] == board[5][2] &&
                    board[3][0] == board[6][3] &&
                    board[3][0] == PLAYER_2 ||

                    board[0][5] == board[1][4] &&
                    board[0][5] == board[2][3] &&
                    board[0][5] == board[3][2] &&
                    board[0][5] == PLAYER_2 ||

                    board[1][4] == board[2][3] &&
                    board[1][4] == board[3][2] &&
                    board[1][4] == board[4][1] &&
                    board[1][4] == PLAYER_2 ||

                    board[2][3] == board[3][2] &&
                    board[2][3] == board[4][1] &&
                    board[2][3] == board[5][0] &&
                    board[2][3] == PLAYER_2 ||

                    board[0][4] == board[1][3] &&
                    board[0][4] == board[2][2] &&
                    board[0][4] == board[3][1] &&
                    board[0][4] == PLAYER_2 ||

                    board[1][3] == board[2][2] &&
                    board[1][3] == board[3][1] &&
                    board[1][3] == board[4][0] &&
                    board[1][3] == PLAYER_2 ||

                    board[0][3] == board[1][2] &&
                    board[0][3] == board[2][1] &&
                    board[0][3] == board[3][0] &&
                    board[0][3] == PLAYER_2 ||

                    board[1][6] == board[2][5] &&
                    board[1][6] == board[3][4] &&
                    board[1][6] == board[4][3] &&
                    board[1][6] == PLAYER_2 ||

                    board[2][5] == board[3][4] &&
                    board[2][5] == board[4][3] &&
                    board[2][5] == board[5][2] &&
                    board[2][5] == PLAYER_2 ||

                    board[3][4] == board[4][3] &&
                    board[3][4] == board[5][2] &&
                    board[3][4] == board[6][1] &&
                    board[3][4] == PLAYER_2 ||

                    board[2][6] == board[3][5] &&
                    board[2][6] == board[4][4] &&
                    board[2][6] == board[5][3] &&
                    board[2][6] == PLAYER_2 ||

                    board[3][5] == board[4][4] &&
                    board[3][5] == board[5][3] &&
                    board[3][5] == board[6][2] &&
                    board[3][5] == PLAYER_2 ||

                    board[3][6] == board[4][5] &&
                    board[3][6] == board[5][4] &&
                    board[3][6] == board[6][3] &&
                    board[3][6] == PLAYER_2

                ) {
                    return true
                }

                for (i in board.indices) {
                    if (
                        board[i][0] == board[i][1] &&
                        board[i][0] == board[i][2] &&
                        board[i][0] == board[i][3] &&
                        board[i][0] == PLAYER_2 ||

                        board[i][1] == board[i][2] &&
                        board[i][1] == board[i][3] &&
                        board[i][1] == board[i][4] &&
                        board[i][1] == PLAYER_2 ||

                        board[i][2] == board[i][3] &&
                        board[i][2] == board[i][4] &&
                        board[i][2] == board[i][5] &&
                        board[i][2] == PLAYER_2 ||

                        board[i][3] == board[i][4] &&
                        board[i][3] == board[i][5] &&
                        board[i][3] == board[i][6] &&
                        board[i][3] == PLAYER_2 ||

                        board[0][i] == board[1][i] &&
                        board[0][i] == board[2][i] &&
                        board[0][i] == board[3][i] &&
                        board[0][i] == PLAYER_2 ||

                        board[1][i] == board[2][i] &&
                        board[1][i] == board[3][i] &&
                        board[1][i] == board[4][i] &&
                        board[1][i] == PLAYER_2 ||

                        board[2][i] == board[3][i] &&
                        board[2][i] == board[4][i] &&
                        board[2][i] == board[5][i] &&
                        board[2][i] == PLAYER_2 ||

                        board[3][i] == board[4][i] &&
                        board[3][i] == board[5][i] &&
                        board[3][i] == board[6][i] &&
                        board[3][i] == PLAYER_2

                    ) {
                        return true
                    }
                }
            }
        }
        return false
    }

    //this function is placing a move in the given cell
    fun placeMove(cell: Cell, player: String) {
        board[cell.i][cell.j] = player
    }
}