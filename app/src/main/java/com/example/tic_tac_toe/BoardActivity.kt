package com.example.tic_tac_toe

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.GridLayout
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_board.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class BoardActivity : AppCompatActivity() {

    private lateinit var dimension: String
    private lateinit var boardCells: Array<Array<ImageView?>>
    val TAG: String = "DIMENSION"

    //creating the board instance
    lateinit var board: Board

    //true = player 1 turn
    var playerTurn: Boolean = true
    var turn: Int = 1

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_board)

        dimension = intent.getStringExtra("dimension")

        Log.i(TAG, dimension)

        board = Board(dimension.toInt())

        boardCells = Array(dimension.toInt()) { arrayOfNulls<ImageView>(dimension.toInt()) }

        if (playerTurn && turn == 1) {
            tvPlayerTurn.text = "Player Turn : Player 1"
            tvTurn.text = "Turn : $turn"
        }
        loadBoard()
    }

    private fun loadBoard() {
        for (i in boardCells.indices) {
            for (j in boardCells.indices) {
                boardCells[i][j] = ImageView(this)
                boardCells[i][j]?.layoutParams = GridLayout.LayoutParams().apply {
                    rowSpec = GridLayout.spec(i)
                    columnSpec = GridLayout.spec(j)
                    if (boardCells.size == 3) {
                        width = 250
                        height = 250
                    } else if (boardCells.size == 5) {
                        width = 165
                        height = 165
                    } else if (boardCells.size == 7) {
                        width = 110
                        height = 110
                    }
                }
                boardCells[i][j]?.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorPrimary
                    )
                )

                //attached a click listener to the board
                boardCells[i][j]?.setOnClickListener(CellClickListener(i, j))
                grid_layout_board.addView(boardCells[i][j])
            }
        }
    }

    //function is mapping
    private fun mapBoardToUi() {
        for (i in board.board.indices) {
            for (j in board.board.indices) {
                when (board.board[i][j]) {
                    Board.PLAYER_1 -> {
                        boardCells[i][j]?.setImageResource(R.drawable.circle)
                        boardCells[i][j]?.isEnabled = false
                    }
                    Board.PLAYER_2 -> {
                        boardCells[i][j]?.setImageResource(R.drawable.cross)
                        boardCells[i][j]?.isEnabled = false
                    }
                    else -> {
                        boardCells[i][j]?.setImageResource(0)
                        boardCells[i][j]?.isEnabled = true
                    }
                }
            }
        }
    }

    inner class CellClickListener(
        private var i: Int,
        private val j: Int
    ) : View.OnClickListener {
        @SuppressLint("SetTextI18n")
        override fun onClick(p0: View?) {
            //checking if the game is not over
            board
            if (!board.isGameOver) {

                //creating a new cell with the clicked index
                val cell = Cell(i, j)

                playerTurn = if (playerTurn) {
                    //placing the move for player1
                    tvPlayerTurn.text = "Player Turn : Player 2"
                    board.placeMove(cell, Board.PLAYER_1)
                    false
                } else {
                    //placing the move for player2
                    tvPlayerTurn.text = "Player Turn : Player 1"
                    board.placeMove(cell, Board.PLAYER_2)
                    true
                }
                //mapping the internal board to visual board
                mapBoardToUi()
            }
            //Displaying the results
            //according to the game status
            when {
                board.hasPlayer1Won() || board.hasPlayer2Won() || board.isGameOver -> showDialog()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDialog() {
        val builder = AlertDialog.Builder(this)
        when {
            board.hasPlayer1Won() -> builder.setMessage(" Result : Player 1 Won!")
            board.hasPlayer2Won() -> builder.setMessage("Result : Player 2 Won!")
            board.isGameOver -> builder.setMessage("Result : Player 2 Won!")
        }
        builder.setPositiveButton("Play again") { dialogInterface: DialogInterface?, i: Int ->
            //creating a new board
            //it will empty every cell
            board = Board(dimension.toInt())
            playerTurn = true
            turn++
            tvPlayerTurn.text = "Player Turn : Player 1"
            tvTurn.text = "Turn : $turn"
            //this function will map the internal board
            mapBoardToUi()
        }
        builder.setNegativeButton("back to home screen") { dialogInterface: DialogInterface, i: Int ->
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        builder.show()
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Are you sure?")
        builder.setMessage("If you exit the game is Over!!")
        builder.setPositiveButton("Yes") { dialogInterface: DialogInterface?, i: Int ->
            super.onBackPressed()
        }
        builder.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int ->
        }
        builder.show()
    }
}